[![pipeline status](https://gitlab.com/ratcash/money-conversion/badges/master/pipeline.svg)](https://gitlab.com/ratcash/money-conversion/commits/master)[![coverage report](https://gitlab.com/ratcash/money-conversion/badges/master/coverage.svg)](https://gitlab.com/ratcash/money-conversion/commits/master)

money-conversion
================

Money conversion library for java-performance.info blog

Sources cloned from https://github.com/mikvor/money-conversion and developed further.
As of version 2.0-SNAPSHOT the major changes are:
* JAVA8 as min-version
* TestNG used for tests
* fixed-precision arithmetics for multiplication and division to be able to stay within MoneyLong as much as possible